<?php

/**
 * Shortcodes
 */

// [k n="1"]
add_shortcode('k', 'disp_keynote_shortcode');
function disp_keynote_shortcode( $atts )
{
	// set defaults
	$atts = shortcode_atts(array(
		'n' => '#',
	), $atts );

	// create the html
	$html  = '<span class="footnote-key" data-index="footnote'. $atts['n'] .'" href="#footnote'. $atts['n'] .'">'. $atts['n'] .'</span>';
	return $html;
}

// [f n="1"] text [/f]
add_shortcode('f', 'disp_footnote_shortcode');
function disp_footnote_shortcode( $atts, $content = null )
{
	// create the html
	$html  = '<div id="'. $atts['n'] .'" class="footnote mb-1 col-6 col-lg-12 mb-2">';
	$html  .= '<span>'. $atts['n'] .'</span>'. $content . '</div>';
	return $html;
}

/**
 * Auto-hyperlink urls for the about_content field.
 */
function my_acf_load_value($value) {
  $value = make_clickable($value);
  return $value;
}
// add_filter('acf/load_value', 'my_acf_load_value', 10, 3);
add_filter('acf/load_value/name=vlastnosti', 'my_acf_load_value', 10, 3);
add_filter('acf/load_value/name=techniky', 'my_acf_load_value', 10, 3);
add_filter('acf/load_value/name=puvod', 'my_acf_load_value', 10, 3);
add_filter('acf/load_value/name=aplikace', 'my_acf_load_value', 10, 3);
add_filter('acf/load_value/name=dostupnost', 'my_acf_load_value', 10, 3);



// [ buttons ]
// add_shortcode('button', 'disp_button_shortcode');
function disp_button_shortcode( $atts, $text = 'enter text' )
{
	// set defaults
	$atts = shortcode_atts(array(
		'color' => '',
		'link'  => '#',
	), $atts );

	// create css class from color
	if ( $atts['color'] ) $atts['color'] = 'btn-'.$atts['color'];

	// outside or internal link?
	$parsed = wp_parse_url( $atts['link'] );
	if ( !isset($parsed['scheme']) ) {
		$atts['link'] = home_url( $atts['link'] );
	}

	// create the html
	$html  = '<a href="'. $atts['link'] .'" class="btn '. $atts['color'] .' animate">';
	$html .= 	$text;
	$html .= '</a>';

	return $html;
}

// [ simple_gallery ]
// add_shortcode('simple_gallery', 'simple_gallery_shortcode');
function simple_gallery_shortcode( $atts, $text = 'enter text' )
{
	// extract variables and set defaults
	$atts = shortcode_atts(array(
		'gallery_class' => 'image-grid group',
		'img_class'     => 'gallery-img',
	), $atts );

	$post  = get_post();
	$media = get_attached_media( 'image', $post->ID );

	if ( ! $media ) return '';

	$html = '<div class="'. esc_attr($atts['gallery_class']) .'">';
	foreach ( $media as $img )
	{
		$html .= '<img src="'. esc_url( wp_get_attachment_image_url($img->ID, 'full') ) .'"
					class="'. esc_attr( $atts['img_class'] ) .'"
					alt="'. esc_attr( $img->post_title ) .'">';
	}
	$html .= '</div>';

	return $html;
}


// Category tree
//         ----smazat----

function build_custom_category_tree ($activeCatId, $activeParentId, $parentId = 0) {

    $output = '';
    $terms = get_terms( array(
        'taxonomy' => 'kategorie',
        'hide_empty' => true,
        'hierarchical' => true,
        'parent' => $parentId
    ) );
		echo "<pre>"; print_r( $terms ); echo "</pre>";

    if (count($terms)) {

        $output .= '<ul>';

        foreach ($terms as $term) {
            $output .= '<li class="custom-cat' . ($term->term_id === $activeParentId || $term->term_id === $activeCatId ? ' active' : '') . '">';
            $output .=  $term->name;
            $output .=  build_custom_category_tree($activeCatId, $activeParentId, $term->term_id);
            $output .= '</li>';
        }

        $output .= '</ul>';
    }

    return $output;
}

/**
 * get_tax_level uroven taxonomie
 */
function get_tax_level($id, $tax){
    $ancestors = get_ancestors($id, $tax);
    return count($ancestors)+1;
}
/**
 * get_level_from_title($title)
 */
function get_level_from_title($title) {
	$level = 3;
	if ( substr($title, 6, 2) == "00" )   $level = 2;
	if ( substr($title, 3, 2) == "00" )   $level = 1;
	return $level;
}
