<?php

/**
 * Add scripts & styles
 */
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

add_action('wp_enqueue_scripts', 'disp_theme_scripts');
function disp_theme_scripts()
{
	wp_enqueue_script(
		'disp-app', get_template_directory_uri() . '/js/header.js',
		array( 'jquery' ), '', true
	);

	wp_enqueue_style(
		'disp-style', get_stylesheet_uri()
	);

	wp_enqueue_style(
		'disp-animations', get_template_directory_uri() . '/css/animations.css'
	);

	wp_enqueue_style(
		'disp-fonts', 'http://fonts.googleapis.com/css?family=Montserrat:400,700'
	);

	if ( is_singular( 'material' ) ) {
		wp_enqueue_script(
			'material-header', get_template_directory_uri() . '/js/material-header.js',
				array( 'jquery', 'bootstrap-js' ), '', true
		);
	}

	if ( is_tax('hashtag') || is_search() ) {
		wp_enqueue_script(
			'archive-featimg', get_template_directory_uri() . '/js/archive-featimg.js',
			array( 'jquery' ), '', true
		);
	}

	// smooth-scrollbar script
	if ( is_front_page() ) {
		wp_enqueue_script( 'smooth-scrollbar', get_template_directory_uri() . '/js/smooth-scrollbar.js', true );
	}

	// only load CONTACT FORM 7 on contact page
	if ( is_page('contact') )
	{
		if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
			wpcf7_enqueue_scripts();
		}

		if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
			wpcf7_enqueue_styles();
		}
	}
}

/**
 * Load bootstrap from CDN
 * https://getbootstrap.com/
 * <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
 * Added functions to add the integrity and crossorigin attributes to the style and script tags.
 */
function enqueue_load_bootstrap() {
    // Add bootstrap CSS
    wp_register_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, NULL, 'all' );
    wp_enqueue_style( 'bootstrap-css' );

		// Add custom style
		wp_register_style( 'custom-css', get_template_directory_uri() . '/css/custom-style.css', ['bootstrap-css'], NULL, 'all' );
		wp_enqueue_style( 'custom-css' );

		// Add bootstrap js
		wp_register_script( 'bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', ['jquery'], NULL, true );
		wp_enqueue_script( 'bootstrap-js' );

    // Add popper js
    wp_register_script( 'popper-js', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', ['jquery'], NULL, true );
    wp_enqueue_script( 'popper-js' );
}

// Add integrity and cross origin attributes to the bootstrap css.
function add_bootstrap_css_attributes( $html, $handle ) {
    if ( $handle === 'bootstrap-css' ) {
        return str_replace( '/>', 'integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />', $html );
    }
    return $html;
}
// add_filter( 'style_loader_tag', 'add_bootstrap_css_attributes', 10, 2 );

// Add integrity and cross origin attributes to the bootstrap script.
function add_bootstrap_script_attributes( $html, $handle ) {
    if ( $handle === 'bootstrap-js' ) {
        return str_replace( '></script>', ' integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>', $html );
    }
    return $html;
}
add_filter('script_loader_tag', 'add_bootstrap_script_attributes', 10, 2);

// Add integrity and cross origin attributes to the popper script.
function add_popper_script_attributes( $html, $handle ) {
    if ( $handle === 'popper-js' ) {
        return str_replace( '></script>', ' integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>', $html );
    }
    return $html;
}
add_filter('script_loader_tag', 'add_popper_script_attributes', 10, 2);

add_action( 'wp_enqueue_scripts', 'enqueue_load_bootstrap' );
