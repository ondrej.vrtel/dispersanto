<?php

/**
 * Setup Theme
 */
add_action( 'after_setup_theme', 'disper_setup' );
function disper_setup()
{
	/**
	 * Theme Support
	 */
	add_theme_support( 'menus' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );


	/**
	 * Menus
	 */
	register_nav_menus(array(
		'primary' => 'Primary Menu',
	));

	/**
	 * Custom post type ORDERING
	 */
	 add_action( 'pre_get_posts' , 'my_search_order' );
	 function my_search_order( $query ) {
		 if($query->is_archive) {
			 $query->set( 'orderby', 'title' );
			 $query->set( 'order', 'ASC' );
		 }
		 return $query;
	 }

	/**
	 * Custom TAX ORDERING
	 */
	 // add_action( 'pre_get_posts' , 'my_tax_order' );
	 function my_tax_order( $query ) {
		 if ( $query->is_tax() && $query->is_main_query() ) {
			 $query->set( 'orderby', 'title' );
			 $query->set( 'order', 'ASC' );
		 }
		 return $query;
	 }

	/**
	 * Editor Style
	 */
	add_editor_style( 'css/editor-style.css' );
}


// Add specific CSS class by filter
// add_filter( 'body_class', 'disper_class_names' );
function disper_class_names( $classes )
{
	$slug = get_post_field( 'post_name', get_post() );

	$classes = array( $slug );

	return $classes;
}


// Turn on 'Kitchen Sink' for all users
// add_filter( 'tiny_mce_before_init', 'disper_unhide_kitchensink' );
function disper_unhide_kitchensink( $args )
{
	$args['wordpress_adv_hidden'] = false;
	return $args;
}


// Add buttons to the Editor
// add_filter('mce_buttons_2', 'disper_more_mce_buttons');
function disper_more_mce_buttons( $buttons )
{
	$buttons[] = 'hr';
	// $buttons[] = 'del';
	// $buttons[] = 'sub';
	// $buttons[] = 'sup';
	// $buttons[] = 'anchor';
	// $buttons[] = 'fontselect';
	// $buttons[] = 'fontsizeselect';
	// $buttons[] = 'cleanup';
	// $buttons[] = 'styleselect';

	return $buttons;
}
