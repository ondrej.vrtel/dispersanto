<?php

/**
 * Disable Garbage Scripts
 */
add_action('init', 'disp_disable_garbage', 9999);
function disp_disable_garbage()
{
	/**
	 * disable emojis
	 */
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

	// filter to remove TinyMCE emojis
	add_filter( 'tiny_mce_plugins', 'disp_disable_emojicons_tinymce' );


	/**
	 * disabled o embed
	 */
	// Remove the REST API endpoint.
	remove_action('rest_api_init', 'wp_oembed_register_route');

	// Turn off oEmbed auto discovery.
	// Don't filter oEmbed results.
	remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

	// Remove oEmbed discovery links.
	remove_action('wp_head', 'wp_oembed_add_discovery_links');

	// Remove oEmbed-specific JavaScript from the front-end and back-end.
	remove_action('wp_head', 'wp_oembed_add_host_js');

	// Remove the REST API lines from the HTML Header
	remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );


	/**
	 * wp_head cleanup
	 */
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('do_feed_rdf', 'do_feed_rdf', 10, 1);
	remove_action('do_feed_rss', 'do_feed_rss', 10, 1);
	//remove_action('do_feed_rss2', 'do_feed_rss2', 10, 1);
	//remove_action('do_feed_atom', 'do_feed_atom', 10, 1);
	remove_action('wp_head', 'feed_links_extra', 3 );
	remove_action('wp_head', 'feed_links', 2 );
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	remove_action('wp_head', 'noindex', 1);
	remove_action('wp_head', 'rel_canonical');

	// remove WordPress version from RSS feeds
	add_filter('the_generator', '__return_false');

	// disable admin toolbar
	//add_filter( 'show_admin_bar', '__return_false' );

	// remove type and id attributes
	add_filter('style_loader_tag', 'html5_script_style_tags');
	add_filter('script_loader_tag', 'html5_script_style_tags');
	function html5_script_style_tags($tag) {
		$tag = preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
		$tag = preg_replace('~\s+id=["\'][^"\']++["\']~', '', $tag);
		return $tag;
	}
}

// disable emojis in text editor
function disp_disable_emojicons_tinymce( $plugins )
{
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	}

	return array();
}


/**
 * Edit Admin Dashboard
 */
add_action( 'admin_menu', 'disp_edit_admin_menus', 999 );
function disp_edit_admin_menus()
{
	remove_menu_page('edit.php');
	remove_submenu_page('themes.php', 'theme-editor.php');
	remove_submenu_page('plugins.php', 'plugin-editor.php');
}


/**
 * Reorder Dashboard Pages
 */
add_filter('custom_menu_order', 'disp_custom_menu_order');
add_filter('menu_order', 'disp_custom_menu_order');

function disp_custom_menu_order( $__return_true )
{
	return array(
		'index.php', // Dashboard
		'separator1', // --Space--
		'edit.php?post_type=material', // Pages
		// 'edit.php', // Post
		'edit-comments.php', // Comments
		'separator2', // --Space--
		'edit.php?post_type=page', // Pages
		'upload.php', // Media
		'themes.php', // Appearance
		'users.php', // Users
		'plugins.php', // Plugins
		'tools.php', // Tools
		'options-general.php', // Settings
	);
}

/**
 *  remove Add media button from TinyMCE
 */
// remove_action('media_buttons', 'media_buttons');

/**
 * Category tree view in edit post
 */

// Let's stop WordPress re-ordering my categories/taxonomies when I select them
function stop_reordering_my_categories($args) {
    $args['checked_ontop'] = false;
    // $args['orderby'] = 'term_order';
    // $args['order'] = 'ASC';
    return $args;
}

// Let's initiate it by hooking into the Terms Checklist arguments with our function above
add_filter('wp_terms_checklist_args','stop_reordering_my_categories');


/**
 *  pokus řazení term_order
 */

 function wpcf_filter_terms_order( $orderby, $query_vars, $taxonomies ) {
     return $query_vars['orderby'] == 'term_order' ? 'term_order' : $orderby;
 }

 // add_filter( 'get_terms_orderby', 'wpcf_filter_terms_order', 10, 3 );


/**
 *   manage KATEGORY admin column
 */

// add_filter( "manage_kategorie_column", 'adding_kategorie_custom_column', 10, 3 );
function adding_kategorie_custom_column( $content,$column_name,$term_id ) {
	switch( $column_name ){
    case 'cb' :
      echo 'xxx';
      break;
  }
	return $content;
}
