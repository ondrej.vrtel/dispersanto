<?php get_header() ?>

			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post() ?>

					<?php $content = apply_filters( 'the_content', get_the_content() ); ?>

				<?php endwhile ?>
			<?php endif ?>

			<?php
			$recent_posts = wp_get_recent_posts(array(
				'numberposts' => 6,
				'post_type'		=> 'material',
				'post_status' => 'publish'
			));
			$count = 0;
			?>
			<div class="post-list">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12 col-md-6 px-0 article-scroll" data-scrollbar> <!-- Left Column Material -->

							<?php foreach($recent_posts as $post) : ?>

								<?php if ( 0 == $count) : ?>
								<?php get_template_part( 'template/frontpage', 'article' ); ?>

								<article class="px-0 px-md-3 px-lg-5 section-description section-description__left">
									<?php echo $content;  ?>
								</article>

								<?php else : ?>
									<?php get_template_part( 'template/frontpage', 'article' ); ?>
								<?php endif; ?>

							<?php $count++; endforeach; wp_reset_query(); ?>

						</div>

							<div class="col-12 col-md-6 px-0 px-md-3 px-lg-5 section-description section-description__right"> <!-- Right COL with content -->
								<?php echo $content;  ?>
							</div>

						</div> <!-- END row -->
					</div>
				</div>

				<script> Scrollbar.initAll(); </script>

<?php get_footer() ?>
