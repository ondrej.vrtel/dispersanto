(function($) {

	jQuery(".form__field")
		.focus(function() {
			jQuery(".form__label").addClass("formfocus");
		})
		.blur(function() {
			jQuery(".form__label").removeClass("formfocus");
	});


}(jQuery));
