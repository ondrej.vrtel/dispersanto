(function($) {

	var str = $( "h1.entry-title" ).text();
	var form_label = $(".form__label");
	var bounce_box_width = $(".bounce-box").width();

	form_label.html( str );
	$( "h1.entry-title" ).addClass("d-none");

	var form_label_width = form_label.width();

	// console.log(form_label_width + ' box: ' + bounce_box_width);
	if ( form_label_width > bounce_box_width ) form_label.addClass('bounce-on');


	var ofsetComments = $('.row-comments').offset().top;
	var innerHeight = $('.colpost-cont').innerHeight();
	$(function () {
		// console.log($('.row-comments').offset().top -91 );
	    $('#collapsecomments').on('shown.bs.collapse', function (e) {
				if ( $('.row-comments').offset().top > $('.colpost-cont').innerHeight() ) {
					$('.colpost-cont').animate({
						scrollTop: $('.row-comments').offset().top -91
					}, 200);
				}
				// console.log(innerHeight );
				// console.log(ofsetComments );
	    });
	});

}(jQuery));
