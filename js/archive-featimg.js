

/**
 *  hover post -> change feautered image
 */

 (function($) {

   var postImgId = $("#post-image > img")[0],
       dispPost = $("article.post");
   dispPost[0].className += " active";
   // nastav post Image na první post
   postImgId.src = dispPost[0].getAttribute('data-scimage');

   for (var i = 0; i < dispPost.length; i++) {

     dispPost[i].addEventListener("mouseenter", function(event) {
       $(dispPost).removeClass("active");
       var aktualImage = event.target.getAttribute('data-scimage');
       postImgId.src = aktualImage;
       event.target.className += " active";
     });
   }


}(jQuery));
