<?php get_header() ?>

    <main id="main" class="site-main">
      <article id="post-xx" class="post-xx">

        <div class="row">

          <div class="col-12 col-md-4 col-xl-6 px-1 px-md-2 order-last post-images">
            <img src="/wp-content/uploads/img/img1.jpg" alt="" width="100%" class="my-1">
            <img src="/wp-content/uploads/img/img1.jpg" alt="" width="100%" class="my-1">
            <img src="/wp-content/uploads/img/img1.jpg" alt="" width="100%" class="my-1">
          </div>

          <div class="col-12 col-md-8 col-xl-6">

            <h1 class="entry-title font-extra">PLEXIGLASS</h1>

            <div class="row">
              <div class="col-12 col-lg-8">
                <div class="entry-taxonomy">
                  <div class=""><a href="" class="pr-2 font-thin">01. PLAST</a> <a href=""  class="pr-2 font-thin">02. POLYAKRYLAT</a> <a href=""  class="pr-2 font-thin">03. BARVA</a>  <span class="numero">#09</span></div>
                  <div class="small pt-0 hashtags"> <a href="" class="pr-2">#GLAZURA</a> <a href=""  class="pr-2">#BARVA</a> <a href=""  class="pr-2">#AKCE</a> <a href=""  class="pr-2">#TRHÁNÍ</a></div>
                </div>

                <!-- Accordion -->
                <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true"><!-- ACORDEON  -->

                  <!-- Accordion Card -->
                  <div class="card mb-0 mt-3 border-left-0 border-right-0">
                    <div class="card-header collapsed font-thin py-1" role="tab" id="headingOne1" data-toggle="collapse" href="#collapseOne1">
                      <a  data-parent="#accordionEx"  aria-expanded="true" aria-controls="collapseOne1">  VLASTNOSTI </a>
                    </div>
                    <div id="collapseOne1" class="collapse" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
                      <div class="card-body pt-0"> Elest, totas dolorporem facesequo blautemos a consequos es am uta vellibus mo odita volo volliqui ipis vid ma sitatectiis modi </div>
                    </div>
                  </div> <!-- Accordion card -->
                  <div class="card mb-0 border-left-0 border-right-0">
                    <div class="card-header collapsed font-thin py-1" role="tab" id="headingOne2" data-toggle="collapse" href="#collapseOne2">
                      <a  data-parent="#accordionEx"  aria-expanded="true" aria-controls="collapseOne2">  TECHNIKY </a>
                    </div>
                    <div id="collapseOne2" class="collapse" role="tabpanel" aria-labelledby="headingOne2" data-parent="#accordionEx">
                      <div class="card-body pt-0"> Elest, totas dolorporem facesequo blautemos a consequos es am uta vellibus mo odita volo volliqui ipis vid ma sitatectiis modi volo volliqui ipis vid ma sitatectiis modi </div>
                    </div>
                  </div> <!-- Accordion card -->
                  <div class="card mb-0 border-left-0 border-right-0">
                    <div class="card-header collapsed font-thin py-1" role="tab" id="headingOne3" data-toggle="collapse" href="#collapseOne3">
                      <a  data-parent="#accordionEx"  aria-expanded="true" aria-controls="collapseOne3">  PŮVOD </a>
                    </div>
                    <div id="collapseOne3" class="collapse" role="tabpanel" aria-labelledby="headingOne3" data-toggle="collapse" data-parent="#accordionEx" href="#footnote01">
                      <div class="card-body pt-0"> Elest, totas dolorporem facesequo blautemos a consequos es am uta vellibus mo odita volo volliqui ipis vid ma sitatectiis modi </div>
                    </div>
                  </div> <!-- Accordion card -->
                  <div class="card mb-0 border-left-0 border-right-0">
                    <div class="card-header collapsed font-thin py-1" role="tab" id="footnote01" data-toggle="collapse" href="#collapsefootnote01">
                      <a  data-parent="#accordionEx"  aria-expanded="true" aria-controls="collapsefootnote01">  APLIKACE </a>
                    </div>
                    <div id="collapsefootnote01" class="footnote collapse my-1" role="tabpanel" aria-labelledby="footnote01" data-parent="#accordionEx">
                      <div class="card-body pt-0"> <span> 01 </span> et, voloritaquod maxime net pa volo qui doluptur, arum qui cus aut escilit et, accus ped et endaniam et exeritatem as cullit, unturiberum ex et la nimus </div>
                    </div>
                  </div> <!-- Accordion card -->
                </div> <!-- Accordion wrapper -->

                <!-- <div class="container-fluid"> -->
                <div class="row py-3 px-1 justify-content-between">
                  <div class="col post-content">
                    01.02.03 Plexiglass — Fluorine yellowEt elest, totas dolorporem facesequo blautemos a consequos es am uta vellibus mo odita volo volliqui ipis vid ma sitatectiis modi dolupti cone possum <strong>quatur</strong> rersperro ea inctiae si ne suntios ma comnianimusa dis eossinctas am sequi optaerspid millabo repudis suntius, veleseque pe con cum, suntia consequam ut volore magnis evellab oresto dolorem volore doluptatis quas sin perspelibus moluptur? Ficitatibus as accatecti
                    tet eos aut et ommo volupta ssumet harias acescim oluptur, suntisti vel ius, que volorro mi, optio<span class="footnote-key" data-toggle="collapse" href="#collapsefootnote01">01</span> corehendis ipsantem. Et peliquam eum facimin verrovit et atur aut optam aut volupti aditectatem aboribus sequas rem. Quiatquae vendae nus, cullab inverum
                    tet eos aut et ommo volupta ssumet harias acescim oluptur, suntisti vel ius, que volorro mi, optio  corehendis ipsantem. Et peliquam eum facimin verrovit et atur aut optam aut volupti aditectatem aboribus sequas rem. <i>Quiatquae vendae nus, cullab inverum
                    tet eos aut et ommo volupta ssumet harias acescim oluptur, suntisti vel ius, que volorro mi, optio</i> <span class="footnote-key" data-toggle="collapse" href="#footnote02">02</span> corehendis ipsantem. Et peliquam eum facimin verrovit et atur aut optam aut volupti aditectatem aboribus sequas rem. Quiatquae vendae nus, cullab inverum
                    01.02.03 Plexiglass — Fluorine yellowEt elest, totas dolorporem facesequo blautemos a consequos es am uta vellibus mo odita volo volliqui ipis vid ma sitatectiis modi dolupti cone possum quatur rersperro ea inctiae si ne suntios ma comnianimusa dis eossinctas am sequi optaerspid millabo repudis suntius, veleseque pe con cum, suntia consequam ut volore magnis evellab oresto dolorem volore doluptatis quas sin perspelibus moluptur? Ficitatibus as accatecti
                    tet eos aut et ommo volupta ssumet harias acescim oluptur, suntisti vel ius, que volorro mi, optio<span class="footnote-key" data-toggle="collapse" href="#footnote03">03</span> corehendis ipsantem. Et peliquam eum facimin verrovit et atur aut optam aut volupti aditectatem aboribus sequas rem. Quiatquae vendae nus, cullab inverum
                    tet eos aut et ommo volupta ssumet harias acescim oluptur, suntisti vel ius, que volorro mi, optio  corehendis ipsantem. Et peliquam eum facimin verrovit et atur aut optam aut volupti aditectatem aboribus sequas rem. Quiatquae vendae nus, cullab inverum
                    tet eos aut et ommo volupta ssumet harias acescim oluptur, suntisti vel ius, que volorro mi, optio<span class="footnote-key" data-toggle="collapse" href="#footnote04">04</span> corehendis ipsantem. Et peliquam eum facimin verrovit et atur aut optam aut volupti aditectatem aboribus sequas rem. Quiatquae vendae nus, cullab inverum
                  </div>
                </div>

              </div>
              <div class="col-12 col-lg-4 notes"><!-- NOTES -->
                <div class="footnotes sticky-top row">
                  <div id="" class="footnote mb-1 col-6 col-lg-12 mb-2">
                    <span> 01 </span> et, voloritaquod maxime net pa volo qui doluptur, arum qui cus aut escilit et, accus ped et endaniam et exeritatem as cullit, unturiberum ex et la nimus
                  </div>
                  <div id="footnote02" class="footnote mb-1 col-6 col-lg-12 mb-2">
                    <span> 02 </span> maxime net pa volo qui doluptur, arum qui cus aut escilit et, accus ped et endaniam et exeritatem as cullit, unturiberum ex et la nimus sdfsdf sdf
                  </div>
                  <div id="footnote03" class="footnote mb-1 col-6 col-lg-12 mb-2">
                    <span> 03 </span> et, voloritaquod maxime net pa volo qui doluptur, arum qui cus aut escilit et, accus ped et endaniam et exeritatem as cullit, unturiberum ex et la nimus
                  </div>
                  <div id="footnote04" class="footnote mb-1 col-6 col-lg-12 mb-2">
                    <span> 04 </span> maxime net pa volo qui doluptur, arum qui cus aut escilit et, accus ped et endaniam et exeritatem as cullit, unturiberum ex et la nimus sdfsdf sdf
                  </div>
                </div>
              </div>

              </div> <!-- end inner row-->
              <div class="row"><!-- KOMENTÁŘ -->
                  <div class="col">
                    <div class="accordion">
                      <div class="card mb-0 border-left-0 border-right-0">
                        <div class="card-header collapsed font-thin py-1" role="tab" id="comments" data-toggle="collapse" href="#collapsecomments">
                          <a data-parent="#accordionEx"  aria-expanded="true" aria-controls="collapsecomments">  KOMENTÁŘE </a>
                        </div>
                        <div id="collapsecomments" class="footnote collapse my-1" role="tabpanel" aria-labelledby="comments" data-parent="#accordionEx">
                          <div class="card-body pt-0">
                            <p class="nick mb-1"><i>iouda – 23. 12. 2019</i></p>
                            <p> KOMENTÁŘ 1 pa volo qui doluptur, arum qui cus aut escilit et, accus ped et endaniam et exeritatem as cullit, unturiberum ex et la nimus </p>
                            <p class="nick mb-1"><i>iouda2 – 24. 12. 2019</i></p>
                            <p> KOMENTÁŘ 2 pa volo qui doluptur, arum qui cus aut escilit et, accus ped et endaniam et exeritatem as cullit, unturiberum ex et la nimus </p>

                          </div>
                        </div>
                      </div> <!-- Accordion card -->
                    </div>
                  </div>
                </div>
          </div><!-- col -->

        </div> <!-- row -->
      </article>

      <nav class="navigation post-navigation" role="navigation">
        <h2 class="sr-only">Navigace v článcích</h2>
        <div class="nav-links">
          <div class="container-fluid">
            <div class="row">
              <div class="col-6 col-md-4 font-extra nav-previous pl-0"><a href="#" rel="prev">← ŠTUK</a></div>
              <div class="col-6 col-md-4 font-extra nav-next pr-0 order-md-last"><a href="#" rel="next">DŘEVO →</a></div>
              <div class="col col-md-4 my-auto text-center"><a href="#collapsecomments" data-toggle="collapse">KOMENTÁŘE</a></div>
            </div>
          </div>
        </div>
      </nav>


  </main>

  <?php if ( have_posts() ) : ?>
	<div class="post-list">
		<?php while ( have_posts() ) : the_post() ?>

			<article <?php post_class() ?>>
				<h1 class="post-title"><?php the_title() ?></h1>
				<?php the_content('') ?>
			</article>

		<?php endwhile ?>
	</div>
	<?php endif ?>

<?php get_footer() ?>
