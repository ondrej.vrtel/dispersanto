<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
*/
if ( post_password_required() ) {
	return;
}

?>

<div id="comments" class="comments-area">
	<?php if ( have_comments() ) : ?>
		<ol class="commentlist">
		  <?php wp_list_comments(array(
            'style'       => 'ul',
            'format'      => 'html5',
            'short_ping'  => true,
        ) ); ?>
		</ol>
	<?php endif; ?>



	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

		<?php if ( is_user_logged_in() ) : ?>
			<p><?php printf(__('Přihlášen jako %1$s. %2$sOdhlásit &raquo;%3$s', 'theme'), '<a href="'.get_option('siteurl').'/wp-admin/profile.php">'.$user_identity.'</a>', '<a href="'.(function_exists('wp_logout_url') ? wp_logout_url(get_permalink()) : get_option('siteurl').'/wp-login.php?action=logout" title="').'" title="'.__('Odhlásit', 'theme').'">', '</a>') ?></p>
		<?php else : ?>

			<p>
				<label for="author"><?php _e('Name', 'theme') ?> <span><?php if ($req) _e("*", 'theme'); ?></span></label>
				<input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" />
			</p>
			<p>
				<label for="email"><?php _e('Email', 'theme') ?><span> <?php if ($req) _e("*", 'theme'); ?></span></label>
				<input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" />
			</p>
			<p>
				<label for="url"><?php _e('Website', 'theme') ?></label>
				<input type="text" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" size="22" tabindex="3" />
			</p>


	<?php endif; ?>

	<p>
            <label for="comment"><?php _e('Comment', 'theme') ?></label>
            <textarea name="comment" id="comment" cols="58" rows="10" tabindex="4"></textarea>
        </p>

	<p>
            	<button class="com_button" type="submit" name="submit">
                    <?php _e('POST COMMENT', 'theme'); ?>
                </button>
	    <?php comment_id_fields(); ?>
	</p>
	<?php do_action('comment_form', $post->ID); ?>
</form>


</div><!-- #comments -->
