<?php get_header() ?>

<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post() ?>

    <main id="main" class="site-main">
      <article id="post-xx" <?php post_class() ?>>

        <div class="row"> <!-- řádek celé stránky -->

          <!-- col2 images -->
          <?php get_template_part( 'template/article', 'images' ); // Sloupec s obrázky ?>

          <!-- col1 content -->
          <div class="col-12 col-md-8 col-xl-6">
            <!-- article head -->
            <h1 class="entry-title font-extra"><?php echo $post->post_title ?></h1>

						<?php the_content('') ?>

						<!-- ACCORDION -->
						<?php get_template_part( 'template/article', 'accordioninfo' ) ?>

          </div><!-- col -->

        </div> <!-- row -->
      </article>
		</main>


<?php endwhile ?>
<?php endif ?>

<?php get_footer() ?>
