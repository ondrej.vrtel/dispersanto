<!-- template post navigation -->

<nav class="navigation post-navigation" role="navigation">
  <h2 class="sr-only">Navigace v článcích</h2>
  <div class="nav-links">
    <div class="container-fluid">
      <div class="row  "> <!-- d-flex justify-content-between  -->
        <!-- <div class="col-6 col-md-4 font-extra nav-previous pl-0"><a href="#" rel="prev">← ŠTUK</a></div> -->
        <div class="col-6 col-md-4 font-thin nav-previous pl-0"><?php previous_post_link( $format = '%link', $link = '← '.'%title', $in_same_term = false, $excluded_terms = '', $taxonomy = 'kategorie' ); ?></div>
        <!-- <div class="col-6 col-md-4 font-extra nav-next pr-0 order-md-last"><a href="#" rel="next">DŘEVO →</a></div> -->
        <div class="col-6 col-md-4 font-thin nav-next pr-0 order-md-last"><?php next_post_link( $format = '%link', $link = '%title'.' →', $in_same_term = false, $excluded_terms = '', $taxonomy = 'kategorie' ); ?></div>
        <div class="col my-auto font-extra text-center"><a href="#collapsecomments" data-toggle="collapse">KOMENTÁŘE</a></div>
      </div>
    </div>
  </div>
</nav>
<!-- komentar  col col-md-4-->
