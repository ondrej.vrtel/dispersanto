<?php

  $queried = get_queried_object_id();
  $termclass = array(' class="pr-2 font-thin"', ' class="pr-2 font-thin activeterm"');



  echo get_tree( 0, $queried, 1 )[0];

function get_tree( $term_id, $queried, $level ){
  $termclass = array(' class="pr-2 font-thin"', ' class="pr-2 font-thin activeterm"');
  $args = array( 'current_category'	=> 0, 'depth' => 1, 'parent' => $term_id, 'hide_empty' => false, 'taxonomy' => 'kategorie' );
  $tree = get_categories($args);
  $html = '';
  $rectiv = 0;
  if ( ! empty( $tree ) && ! is_wp_error( $tree ) ) {
    foreach ( $tree as $term ) {
      $active = ( $term->term_id == $queried ) ? 1 : 0;
      $number = get_field('number', $term);
      $term_list .= '<a href="' . esc_url( get_term_link( $term ) ) . '"' . $termclass[$active] . '>' . $number . '. ' . $term->name . '</a>';
      $subtree = get_tree( $term->term_id, $queried, $level+1 );
      $term_list .= $subtree[0];
      $rectiv += $active + $subtree[1];
    }
    $html .= '<div class="level'.$level.' ml-3 levels';
    $html .= ( $rectiv && $level>1 ) ? ' active' : '';
    $html .= '">' . $term_list . '</div>';
  }
  // echo 'rectiv: ' . $rectiv;
  return [$html,$rectiv];

}



//  LEVEL 1
// $args = array( 'current_category'	=> 0, 'depth' => 1, 'parent' => '0', 'hide_empty' => false, 'taxonomy' => 'kategorie' );
// $cat_lev1 = get_categories($args);
//
// echo '<div class="level1 levels">';
// foreach ( $cat_lev1 as $term ) {
//   // echo $term->term_id;
//   $number = get_field('number', $term);
//   $styly = $termclass[ $term->term_id == $queried ];
//   $term_list = '<a href="' . esc_url( get_term_link( $term ) ) . '"' . $styly . '>' . $number . '. ' . $term->name . '</a>';
//   echo $term_list . ''; // . ' (' . $term->count . ')<br>';
//
//   // LEVEL 2
//   $args2 = array( 'current_category'	=> 0, 'depth' => 1, 'parent' => $term->term_id, 'hide_empty' => false, 'taxonomy' => 'kategorie' );
//   $cat_lev2 = get_categories($args2);
//   if ( ! empty( $cat_lev2 ) && ! is_wp_error( $cat_lev2 ) ) {
//
//     echo '<div class="level2 ml-3 levels">';
//     foreach ( $cat_lev2 as $term ) {
//       $number = get_field('number', $term);
//       // $styly = ( $term->term_id == $queried ) ? $activetermclass : $termclass;
//       $styly = $termclass[ $term->term_id == $queried ];
//       $term_list = '<a href="' . esc_url( get_term_link( $term ) ) . '"' . $styly . '>' . $number . '. ' . $term->name . '</a>';
//       echo $term_list . ''; // . ' (' . $term->count . ')<br>';
//
//       // LEVEL 3
//       echo get_tree( $term->term_id, $queried, 1 )[0];
//
//     }
//     echo '</div>';
//   }
//   // LEVEL 2 - END
// }
// echo '</div>';

 ?>
