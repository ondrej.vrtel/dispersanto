<!-- article TAXONOMY -->
<div class="entry-taxonomy">
  <!-- <div class=""><a href="" class="pr-2 font-thin">01. PLAST</a> <a href=""  class="pr-2 font-thin">02. POLYAKRYLAT</a> <a href=""  class="pr-2 font-thin">03. BARVA</a>  <span class="numero">#<?php echo get_field('number') ?></span></div> -->
  <div class="">
    <?php
    $get_terms_default_attr = array (
          'taxonomy' => 'kategorie', //empty string(''), false, 0 don't work, and return empty array
          // 'orderby' => 'name',
          // 'order' => 'ASC',
          // 'orderby'  => 'term_order',  //řazení podle terms pluginu!
          // 'hide_empty' => true, //can be 1, '1' too
          // 'exclude_tree' => 'all', //empty string(''), false, 0 don't work, and return empty array
          // 'number' => false, //can be 0, '0', '' too
          // 'offset' => '',
          // 'fields' => 'all',
          // 'name' => '',
          // 'slug' => '',
          'hierarchical' => true, //can be 1, '1' too
          // 'search' => '',
          // 'name__like' => '',
          // 'description__like' => '',
          // 'pad_counts' => true, //can be 0, '0', '' too
          // 'get' => '',
          // 'child_of' => false, //can be 0, '0', '' too
          // 'childless' => false,
          // 'cache_domain' => 'core',
          // 'update_term_meta_cache' => true, //can be 1, '1' too
            // 'meta_key'   => 'order',    // woocomerce
            // 'orderby'	=> 'meta_value_num',   //woocomerce by menu order
          'meta_query' => '',
          'meta_key' => array(),
          'meta_value'=> '',
          'parent' => 0
        );

    // GET kategorie
		$terms = get_the_terms( $post->ID, 'kategorie');

    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
        $count = count( $terms );
        $i = 0;
        $term_list = '';
        foreach ( $terms as $term ) {
          // var_dump( $term ); echo '<br>';
            $number = get_field('number', $term);
            $i++;
            $term_list = '<a href="' . esc_url( get_term_link( $term ) ) . '" alt="' . esc_attr( sprintf( __( 'Zobrazit všechny - %s', 'dispersanto' ), $term->name ) ) . '" class="pr-2 font-thin">' . $number . '. ' . $term->name . '</a>' . $term_list;
            if ( $count != $i ) {
                $term_list .= ' ';
            }
            else {
                // $term_list .= '<span class="numero">#' . get_field('number') . '</span>';
            }
        }
        echo $term_list;
    }

    ?>
  </div>

<!-- /**  * hashtagy  */ -->
  <div class="small pt-0 hashtags">
    <!-- <a href="" class="pr-2">#GLAZURA</a> -->

    <?php
    // $term_list = wp_get_post_terms( $post->ID, 'post_tag', array( 'fields' => 'all' ) );
    $tags = get_the_terms( $post->ID, 'hashtag');
    if ( $tags ) {
      foreach ($tags as $tag)
      {
        echo '<a href="/hashtag/'. $tag->slug .'/" class="pr-2">#' . $tag->name . '</a> ';
      }
    }
    ?>

  </div>
</div>
