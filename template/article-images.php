<!-- template  article-images - sloupec s obrázky -->
<div class="col-12 col-md-4 col-xl-6 px-1 px-md-2 order-last post-images">

  <section data-featherlight-gallery data-featherlight-filter="a" >
  <figure class="fig_img">
    <a href="<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>" >
      <?php echo get_the_post_thumbnail( $post->ID, 'large', array( 'class' => 'my-1' ) ); ?>
    </a>
  </figure>
  <!-- <img src="/wp-content/uploads/img/img1.jpg" alt="" width="100%" class="my-1"> -->
  <?php
    $images = get_field('fotogalerie');
    $vids = get_field('y2b_vids');
    $counter = 1;

    // var_dump($images);
    $size = 'large'; // (thumbnail, medium, large, full or custom size)
    if( $images ): ?>
    <!-- <ul> -->
      <?php foreach( $images as $image_id ): ?>
        <?php
        $image_attributes = wp_get_attachment_image_src( $image_id, 'full' );
        $image_terms = get_post($image_id);
        $image_title = $image_terms->post_title;
        $image_excerpt = $image_terms->post_excerpt;
        $image_content = $image_terms->post_content;
        ?>
        <figure class="fig_img">
          <a href="<?php echo $image_attributes[0]; ?>" >
            <?php echo wp_get_attachment_image( $image_id, $size ); ?>
          </a>
        <?php
        // if ( $image_title ) echo '<figcaption class="fig_title">' . $image_title . '</figcaption>';
        if ( $image_content ) echo '<figcaption class="fig_content">' . $image_content . '</figcaption>';
        ?>
      </figure>
      <?php
      if ( $counter == 1 && $vids ) {
        $vidsarray = explode(", ", $vids);
         foreach( $vidsarray as $video_id ) {
           echo '<figure class="fig_vid">';
           echo '<div class="videoWrapper"><iframe width="560" height="315" src="https://www.youtube.com/embed/'.$video_id.'" frameborder="0" allowfullscreen></iframe></div>';
           echo '</figure>';
         }
      }
      $counter++;
      ?>
      <?php endforeach; ?>
    <!-- </ul> -->
  <?php endif; ?>

</section>

</div>
