
<article> <!-- COL material -->
  <?php $postlink = get_permalink($post['ID']) ?>
  <a href="<?php echo $postlink ?>">
  <?php echo get_the_post_thumbnail($post['ID'], 'full', array( 'class' => 'w-100 h-auto' )); ?>
  </a>
  <h1 class="entry-title font-extra pl-0"><a href="<?php echo $postlink ?>"><span class="font-thinx"><?php echo $term_list?></span><?php echo $post['post_title']; ?></a>
    <span class="numero font-thinx"><?php echo get_field('number', $post['ID']) ?></span></h1>
</article>
