<!-- template Accordion -->
<div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true"><!-- ACORDEON  -->

  <!-- Accordion Card -->
  <?php if ( get_field('vlastnosti') ) : ?>
  <div class="card mb-0 mt-3 border-left-0 border-right-0">
    <div class="card-header collapsed font-thin py-1" role="tab" id="headingOne1" data-toggle="collapse" href="#collapseOne1">
      <a  data-parent="#accordionEx"  aria-expanded="true" aria-controls="collapseOne1">VLASTNOSTI </a>
    </div>
    <div id="collapseOne1" class="collapse" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
      <div class="card-body pt-0"> <?php echo do_shortcode( get_field('vlastnosti')) ?> </div>
    </div>
  </div> <!-- Accordion card -->
  <?php endif ?>

  <?php if ( get_field('techniky') ) : ?>
  <div class="card mb-0 border-left-0 border-right-0">
      <div class="card-header collapsed font-thin py-1" role="tab" id="headingOne2" data-toggle="collapse" href="#collapseOne2">
      <a  data-parent="#accordionEx"  aria-expanded="true" aria-controls="collapseOne2">TECHNIKY </a>
    </div>
    <div id="collapseOne2" class="collapse" role="tabpanel" aria-labelledby="headingOne2" data-parent="#accordionEx">
      <div class="card-body pt-0"> <?php echo do_shortcode( get_field('techniky')) ?> </div>
    </div>
  </div> <!-- Accordion card -->
  <?php endif ?>

  <?php if ( get_field('puvod') ) : ?>
  <div class="card mb-0 border-left-0 border-right-0">
    <div class="card-header collapsed font-thin py-1" role="tab" id="headingOne3" data-toggle="collapse" href="#collapseOne3">
      <a  data-parent="#accordionEx"  aria-expanded="true" aria-controls="collapseOne3">PŮVOD </a>
    </div>
    <div id="collapseOne3" class="collapse" role="tabpanel" aria-labelledby="headingOne3"  data-parent="#accordionEx">
      <div class="card-body pt-0"> <?php echo do_shortcode( get_field('puvod')) ?> </div>
    </div>
  </div> <!-- Accordion card -->
  <?php endif ?>

  <?php if ( get_field('aplikace') ) : ?>
  <div class="card mb-0 border-left-0 border-right-0">
    <div class="card-header collapsed font-thin py-1" role="tab" id="headingOne4" data-toggle="collapse" href="#collapseOne4">
      <a  data-parent="#accordionEx"  aria-expanded="true" aria-controls="collapseOne4">APLIKACE </a>
    </div>
    <div id="collapseOne4" class="footnote collapse my-1" role="tabpanel" aria-labelledby="headingOne4" data-parent="#accordionEx">
      <div class="card-body pt-0"> <?php echo do_shortcode( get_field('aplikace')) ?> </div>
    </div>
  </div> <!-- Accordion card -->
  <?php endif ?>

  <?php if ( get_field('dostupnost') ) : ?>
  <div class="card mb-0 border-left-0 border-right-0">
    <div class="card-header collapsed font-thin py-1" role="tab" id="headingOne5" data-toggle="collapse" href="#collapseOne5">
      <a  data-parent="#accordionEx"  aria-expanded="true" aria-controls="collapseOne5">DOSTUPNOST </a>
    </div>
    <div id="collapseOne5" class="footnote collapse my-1" role="tabpanel" aria-labelledby="headingOne5" data-parent="#accordionEx">
      <div class="card-body pt-0"> <?php echo do_shortcode( get_field('dostupnost')) ?> </div>
    </div>
  </div> <!-- Accordion card -->
  <?php endif ?>


</div> <!-- Accordion wrapper -->
