<?php


$args2 = array(
  // 'type'                     => 'post',
  'child_of'                => 0,  // 3 zobrazí jen polymery
  'current_category'    		=> 0,
  'depth'               		=> 3,
  'parent'                  => '', // rubriky jen s tímto rodičem
  // 'orderby'                  => 'term_order',
  'order'                    => 'ASC',
  'hide_empty'              => 0,
  'hide_title_if_empty'			=> 1,
  'show_count'							=> 1,
  'show_option_all'					=> '',  // položka pro odkaz na všechny kategorie
  'separator'								=> '<br />',
  'style'										=> 'list',
  // 'use_desc_for_title'			=> 1, // zda se má popis zobrazit v title
  'hierarchical'            => 1,
  'exclude'                 => '',
  'include'                 => '',
  'number'                   => '',
  'taxonomy'                 => 'kategorie',
  'pad_counts'               => false
);

wp_list_categories( $args2 );

?>
