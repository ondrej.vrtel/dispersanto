<?php ?>


<article id="post-<?php the_ID(); ?>" class="post"  data-scimage="<?php echo get_the_post_thumbnail_url(); ?>">
	<header class="entry-header">
		<?php
		if ( is_sticky() && is_home() && ! is_paged() ) {
			printf( '<span class="sticky-post">%s</span>', _x( 'Featured', 'post', 'twentynineteen' ) );
		}
		the_title( sprintf( '<h1 class="entry-title font-extra"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' );
		?>
	</header><!-- .entry-header -->

	<div class="entry-content small">
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->

</article>
