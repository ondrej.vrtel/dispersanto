<?php get_header() ?>

<?php
$queriedtax = $wp_query->get_queried_object();
$current_term_level = get_tax_level(get_queried_object()->term_id, $queriedtax->taxonomy);
$hashname = array();
$hashslug = array();
 ?>

			<div class="post-list">
				<div class="container-fluid">
					<div class="row">
						<div class="col"> <!-- COL 1 -->

							<?php get_template_part( 'template/category', 'tree' ); ?>

						</div>
						<div class="col layer<?php echo $current_term_level  ?>">  <!-- COL 2 -->

							<?php
								 $number = get_field('number', $queriedtax);
							   // echo ''. $number .'. '. $queriedtax->name . '';
							   // echo "<br>";
							   // echo ''. $queriedtax->description .'';

                 global $query;
                 echo "<pre>"; print_r( $query ); echo "</pre>";

							?>

							<?php if ( have_posts() ) : ?>
								<?php while ( have_posts() ) : the_post() ?>

									<div class="post-title layer<?php echo get_level_from_title($post->post_title) ?>"><a href="<?php echo esc_url(get_permalink($post->ID)); ?>"><?php echo apply_filters( 'the_title', $post->post_title ) ?></a> </div>
                    <!-- <?php echo get_level_from_title($post->post_title) ?> -->

									<?php
							    // hashtagy
							    $tags = get_the_terms( $post->ID, 'hashtag');
							    if ( $tags ) {
							      foreach ($tags as $tag)
							      {
                      // echo "<pre>"; print_r($tag->name); echo "</pre>";

                      array_push($hashname, $tag->name);
                      array_push($hashslug, $tag->slug);

							        // echo '<a href="/hashtag/'. $tag->slug .'/" class="pr-2">#' . $tag->name . '</a>';
							      }
							    }
							    ?>
								<?php endwhile ?>
							<?php endif ?>
							<!-- <?php get_template_part( 'template/category', 'list-tree' ); ?> -->
						</div>
						<div class="col">  <!-- COL 3 -->
							<?php
								echo '#HASHTAG<BR>';
                echo '<div class="small pt-0 hashtags">';
                $uniquename = array_unique($hashname);
                $uniqueslug = array_unique($hashslug);
                array_multisort($uniquename, $uniqueslug);
                // echo "<pre>"; print_r($uniquename); echo "</pre>";
                $i = 0;
                foreach ($uniqueslug as $key=>$val) {
                  // echo 'tag: '. $key . ' val: ' . $val . '<br>';
                  echo '<a href="/hashtag/'. $val .'/" class="pr-2">#' . $uniquename[$key] . '</a><br>';
                  $i++;
                };
                echo '</div>';
							?>
						</div>
					</div>
				</div>
			</div>


<?php get_footer() ?>
