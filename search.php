<?php get_header() ?>

<div class="post-list">
	<div class="container-fluid">

		<header class="page-header mb-2">
			<h2 class="page-title">
				<?php _e( 'Výsledek vyhledávání: ', 'disp' ); ?>
					<span class="page-description"><?php echo get_search_query(); ?></span>
				</h2>
			</header><!-- .page-header -->

		<?php if ( have_posts() ) : ?>

		<div class="row">
			<div class="col-8"> <!-- COL 1 -->

				<?php
				// Start the Loop.
				while ( have_posts() ) :
				the_post();

				get_template_part( 'template/article', 'grid' );

				// End the loop.
				endwhile;

				else :
				// If no content, include the "No posts found" template.
				get_template_part( 'template/search', 'none' );

			endif; ?>

			</div>
			<div class="col-4" id="post-image">  <!-- COL 2 -->
				<img src="" alt="" width="100%">
			</div>
		</div>
	</div>
</div>


<?php get_footer();
