<?php get_header() ?>

<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post() ?>

    <main id="main" class="site-main">
      <article id="post-xx" <?php post_class() ?>>

        <div class="row"> <!-- řádek celé stránky -->

          <!-- col2 images -->
          <?php get_template_part( 'template/article', 'images' ); // Sloupec s obrázky ?>

          <!-- col1 content -->
          <div class="col-12 col-md-8 col-xl-6 colpost-cont">
            <!-- article head -->
            <h1 class="entry-title font-extra"><?php echo $post->post_title ?></h1>

            <div class="row">
              <div class="col-12 col-lg-8"> <!-- col1 CONTENT  -->
                <!-- TAXONOMY -->
                <?php get_template_part( 'template/article', 'taxonomy' ) ?>

                <!-- ACCORDION -->
                <?php get_template_part( 'template/article', 'accordion' ) ?>

                <!-- CONTENT -->
                <div class="row py-3 px-1 justify-content-between">
                  <div class="col post-content">
                    <?php the_content('') ?>
                  </div>
                </div>
              </div>

              <div class="col-12 col-lg-4 notes"><!-- col2 NOTES -->
                <div class="footnotes sticky-top row">
                  <?php echo do_shortcode( get_field('poznamky')) ?>
                </div>
              </div>

              </div> <!-- end inner row-->

              <div class="row mb-5 row-comments"><!-- KOMENTÁŘ -->
                  <div class="col">
                    <div class="accordion">
                      <div class="card mb-0 border-0">
                        <div id="collapsecomments" class="footnote collapse my-1" role="tabpanel" aria-labelledby="comments" data-parent="#accordionEx">
                          <div class="card-body pt-0">
                            <?php comments_template(); ?>
                          </div>
                        </div>
                        <!-- <div class="card-header collapsed font-thin py-1" role="tab" id="comments" data-toggle="collapse" href="#collapsecomments">
                          <a data-parent="#accordionEx"  aria-expanded="true" aria-controls="collapsecomments">  KOMENTÁŘE </a>
                        </div> -->
                      </div> <!-- Accordion card -->
                    </div>
                  </div>
                </div>
          </div><!-- col -->

        </div> <!-- row -->
        <?php endwhile ?>
      <?php endif ?>
      </article>

      <?php get_template_part( 'template/article', 'postnav' ) ?>


  </main>

<?php get_footer() ?>
