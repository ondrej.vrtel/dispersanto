<?php get_header() ?>

			<?php
			$recent_posts = wp_get_recent_posts(array(
				'numberposts' => 1,
				'post_type'		=> 'material',
				'post_status' => 'publish'
			));
			foreach($recent_posts as $post) : ?>

			<?php $postlink = get_permalink($post['ID']) ?>

			<div class="post-list">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12 col-md-6 px-0 "> <!-- COL 1 -->
							<a href="<?php echo $postlink ?>">
							<?php echo get_the_post_thumbnail($post['ID'], 'full', array( 'class' => 'w-100 h-auto' )); ?>
							</a>
						</div>
						<div class="col col-md-6 px-0 cover" style="max-height: 80%">  <!-- COL 2 -->
							<a href="<?php echo $postlink ?>">
							<?php
								$images = get_field('fotogalerie', $post['ID']);
						    $size = 'full'; // (thumbnail, medium, large, full or custom size)
						    if( $images ): ?>
								<?php foreach( $images as $image_id ): ?>
										<?php echo wp_get_attachment_image( $image_id, $size, "", array( 'class' => 'w-100 h-100 cover' ) ); break;?>
								<?php endforeach; ?>
								<?php endif; ?>
							</a>
						</div>
					</div>
			</div>
		</div>
		<footer class="footer fixed-bottom">
      <div class="container-fluid ">
				<h1 class="entry-title font-extra pl-0"><a href="<?php echo $postlink ?>"><span class="font-thinx"><?php echo $term_list?></span><?php echo $post['post_title']; ?></a>
					<span class="numero font-thinx"><?php echo get_field('number', $post['ID']) ?></span></h1>
      </div>
    </footer>

		<?php endforeach; wp_reset_query(); ?>

<?php get_footer() ?>
