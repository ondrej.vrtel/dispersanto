<!DOCTYPE html>
<html lang="cs-CZ">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('sitename') ?> Feed" href="<?php echo get_bloginfo('rss2_url') ?>">
	<?php wp_head() ?>
</head>
<body <?php body_class() ?>>

	<header class="site-header">
		<!-- NAVBAR -->
		<nav class="container-fluid">
			<div class="row">
				<div class="navbar-header col-12 col-md-6">
					<!-- <a class="navbar-brand font-extra text-muted" href="/">DISPERSANTO|</a> -->
					<?php  get_search_form( );?>
					<!-- <div class="form__group field">
					  <input type="search" class="form__field" placeholder="DISPERSANTO" name="name" id='name' required />
					  <label for="name" class="form__label">DISPERSANTO</label>
					</div> -->
				</div>
				<div class="main-menu col-12 col-md-6 d-flex justify-content-end">
					<div class="menu-item current-menu-item col-6 col-md-auto pl-0">
						<a class="nav-link p-0" href="/strom/">STROM</a>
					</div>
					<div class="menu-item col-6 col-md-auto text-right pr-0">
						<a class="nav-link p-0" href="/informace/">INFORMACE</a>
					</div>
				</div>
			</div>
		</nav>
	</header>

	<main>
		<section class="container-fluid p-3 m-0">
